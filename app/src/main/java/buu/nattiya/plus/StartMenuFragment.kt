package buu.nattiya.plus

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.nattiya.plus.databinding.FragmentStartMenuBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


/**
 * A simple [Fragment] subclass.
 * Use the [StartMenuFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class StartMenuFragment : Fragment() {
    lateinit var binding: FragmentStartMenuBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_start_menu , container, false)
        binding.btnPlus.setOnClickListener {
            it.findNavController().navigate(R.id.action_fragmentStartmenu_to_fragmentPlus)
        }
        binding.btnMinus.setOnClickListener {
            it.findNavController().navigate(R.id.action_fragmentStartmenu_to_fragmentMinus)
        }
        binding.btnMul.setOnClickListener {
            it.findNavController().navigate(R.id.action_fragmentStartmenu_to_fragmentMul)
        }
        return binding.root
    }
}