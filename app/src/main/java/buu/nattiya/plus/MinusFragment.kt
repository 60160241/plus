package buu.nattiya.plus

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.nattiya.plus.databinding.FragmentMinusBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MinusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MinusFragment : Fragment() {
    private lateinit var binding: FragmentMinusBinding
    private var resultText = "Please select an answer"
    private var scoreNum = Score()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        binding =  DataBindingUtil.inflate(inflater, R.layout.fragment_minus, container, false)
        binding.resultText = resultText
        binding.score = scoreNum
        play()
        binding.apply {
            btnNext.setOnClickListener {
                txtAnswer.setText("Please Select an Answer")
                play()
            }
        }
        binding.btnBack.setOnClickListener {
            it.findNavController().navigate(R.id.action_fragmentMinus_to_fragmentStartmenu)
        }
        return binding.root
    }
    fun play() {
        binding.apply {
            val random1: Int = Random.nextInt(10) + 1
            txtQ1.setText(Integer.toString(random1))

            val random2: Int = Random.nextInt(10) + 1
            txtQ2.setText(Integer.toString(random2))

            val sum = random1 - random2

            val position: Int = Random.nextInt(3) + 1

            if (position == 1) {
                btn1.setText(Integer.toString(sum))
                btn2.setText(Integer.toString(sum + 1))
                btn3.setText(Integer.toString(sum - 1))
            } else if (position == 2) {
                btn1.setText(Integer.toString(sum))
                btn2.setText(Integer.toString(sum + 2))
                btn3.setText(Integer.toString(sum - 2))
            } else {
                btn1.setText(Integer.toString(sum))
                btn2.setText(Integer.toString(sum + 3))
                btn3.setText(Integer.toString(sum - 3))
            }

            btn1.setOnClickListener {
                if (btn1.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    scoreNum.correct++
                    //txtAnswer.setText("Correct")
                    // txtCorrect.text = (txtCorrect.text.toString().toInt() + 1).toString()
                } else {
                    resultText = "Wrong"
                    scoreNum.wrong++
                    // txtAnswer.setText("Wrong")
                    // txtWrong.text = (txtWrong.text.toString().toInt() + 1).toString()
                }
                binding.invalidateAll()
            }

            btn2.setOnClickListener {
                if (btn2.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    scoreNum.correct++
                    //txtAnswer.setText("Correct")
                    //txtCorrect.text = (txtCorrect.text.toString().toInt() + 1).toString()
                } else {
                    resultText = "Wrong"
                    scoreNum.wrong++
                    //txtAnswer.setText("Wrong")
                    //txtWrong.text = (txtWrong.text.toString().toInt() + 1).toString()
                }
                binding.invalidateAll()
            }

            btn3.setOnClickListener {
                if (btn3.text.toString().toInt() == sum) {
                    resultText = "Correct"
                    scoreNum.correct++
                    // txtAnswer.setText("Correct")
                    //txtCorrect.text = (txtCorrect.text.toString().toInt() + 1).toString()
                } else {
                    resultText = "Wrong"
                    scoreNum.wrong++
                    //txtAnswer.setText("Wrong")
                    //txtWrong.text = (txtWrong.text.toString().toInt() + 1).toString()
                }
                binding.invalidateAll()
            }
        }
    }
}